<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Accueil - Amadou Dieng de Labé </title>
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
</head>
<body>
    <header>
        <div class="logo">
            <img src="{{ asset('images/logo.jpg') }}" alt="">
        </div>
        <nav>
            <a href="#">Accueil</a>
            <a href="#">Campus</a>
            <a href="#">Formations</a>
            <a href="#">A propos</a>
            <a href="#">Contact</a>
            <a href="#">S'inscrire</a>
        </nav>

    </header>

    <main>
        <div class="ban">

        </div>
        
        <div class="mot">
            <img src="{{ asset('images/recteur/recteur.jpg')}}" alt="">
            <div>
                <h1>Mot du recteur</h1>
                <article>
                    êtus d’une tunique bleue-blanche, à l’effigie des couleurs de leur logo, les Étudiants de la 10ème cohorte de l’Institut de Formation Professionnelle Ahmadou DIENG (IFPAD) et de la 13ème cohorte de l’Université Ahmadou DIENG ont officiellement reçu leurs diplômes de fin d’étude ce 5 février 2023. C’était en marge d’une cérémonie organisée au stade Petit Sory de Nongo sous le parrainage de Hadja Aicha BAH, ancienne ministre de l’Enseignement Pré-Universitaire et de l’Alphabétisation, et dont les deux cohortes portent désormais le nom.
                    Ils sont au total 825 Étudiants, dont 468 admis sur 474 inscrits à l’IFPAD et 357 admis sur 366 des départements de Santé, Droit, Banques– Finance et Assurance, Commerce International, Administration des Affaires, Économie, Science comptable, Sociologie, Génie Géologie, Génie Minier, Génie Civil et Génie Informatique à recevoir leurs diplômes. Parmi eux, figure feu Mamadou Dian DIALLO, Étudiant en Génie Civil, dont le diplôme a été délivré aux parents à titre posthume.
                </article>
            </div>
        </div>

        <!-- Dicipline  -->

        <div class="formations">
            <h1>Nos formations</h1>
            <div class="formation">
                @foreach($formations as $formation)
                <div style="background-image: url('{{asset('storage/formations/'.$formation->image)}}');
                background-size: cover;
                background-repeat: no-repeat;"><span>{{$formation->formation}}</span></div>
                @endforeach
            </div>
        </div>

        <!-- Fin discipline -->
    </main>

    <footer>
        <div>
            <img src="{{ asset('images/logo.png')}}" alt="">
            <div>
                <div class="ad">
                    <i class="fa fa-phone"></i>
                    <span><a href="tel:+224620717339">+224 620 717 339</a></span>
                </div>
                
                <div class="ad">
                    <i class="fa fa-phone"></i>
                    <span><a href="mailto:campuslabe@univadieng.com">campuslabe@univadieng.com</a></span>
                </div>
                
                <div class="ad">
                    <i class="fa fa-map-marker"></i>
                    <span>ndjiolou, Labé, Guinea</span>
                </div>
                
            </div>
        </div>
        <div></div>
        <div>
            <h2>Liens rapides</h2>
            <nav>
                <a href="#">Accueil</a>
                <a href="#">Campus</a>
                <a href="#">Formations</a>
                <a href="#">A propos</a>
                <a href="#">Contact</a>
                <a href="#">S'inscrire</a>
            </nav>
        </div>
    </footer>
    
</body>
</html>