@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Ajouter une formation</h1>

    <form action="{{route('formation.store')}}" method="POST" enctype="multipart/form-data"> 
        @csrf
        @include('formation._form')
        <input type="submit" value="Enregistrer" class="btn btn-success">
    </form>
</div>
@endsection