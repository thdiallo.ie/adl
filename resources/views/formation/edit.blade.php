@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Ajouter une formation</h1>

    <form action="{{route('formation.update', $formation->id)}}" method="POST" enctype="multipart/form-data"> 
        @csrf
        @method('PUT')
        @include('formation._form')
        <input type="submit" value="Enregistrer" class="btn btn-success">
    </form>
</div>
@endsection