<div class="form-group">
    <label for="formation">Formation</label>
    <input type="text" value="{{ old('formation') ? old('formation') : $formation->formation }}" name="formation" class="form-control" id="formation" placeholder="Entrer le nom de la formation">
    @error('formation') <span style="color:red">{{$message}}</span> @enderror
</div>

<div class="form-group">
    <label for="description">Description</label>
    <textarea type="text" name="description" class="form-control" id="description" placeholder="Entrer le nom de la formation">{{ old('description') ? old('description') : $formation->description }}</textarea>
    @error('description') <span style="color:red">{{$message}}</span> @enderror
</div>

@if($formation->id==0)
<div class="form-group">
    <label for="image">Image</label>
    <input type="file" name="image" class="form-control" id="image" placeholder="Entrer le nom de la formation">
    @error('image') <span style="color:red">{{$message}}</span> @enderror
</div>
@else

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="image">Image</label>
            <input type="file" name="image" class="form-control" id="image" placeholder="Entrer le nom de la formation">
            @error('image') <span style="color:red">{{$message}}</span> @enderror
        </div>
    </div>
    <div class="col-md-6">
        <img src="{{ asset('storage/formations/'.$formation->image)}}" alt="">
    </div>
</div>

@endif

