@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Liste des formations</h1>
    <a href="{{route('formation.create')}}" class="btn btn-primary">Ajouter une formation</a>
    <table class="table table-stripped table-bordered">
        <tr>
            <th>#</th>
            <th>Formation</th>
            <th>Image</th>
            <th>Description</th>
            <th>Action</th>
        </tr>

        @foreach($formations as $key => $formation)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$formation->formation}}</td>
            <td>
                <img style="width:100px" src="{{ asset('storage/formations/'.$formation->image)}}" alt="">
            </td>
            <td>{{$formation->description}}</td>
            <td>
                <a href="{{route('formation.edit', $formation->id)}}" class="btn btn-warning">Modifier</a>
                <form action="{{route('formation.destroy', $formation->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input onclick="confirm('Voulez-vous vraiment supprimer cette formation')" type="submit" value="Supprimer" class="btn btn-danger">
                </form>
            </td>
        </tr>
        @endforeach
    </table>

</div>
@endsection