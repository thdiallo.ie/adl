<?php

namespace App\Http\Controllers;
use App\Models\Formation;

use Illuminate\Http\Request;

class FormationController extends Controller
{
    public function accueil(){
        $formations = Formation::all();
        return view('labe', compact('formations'));
    }

    public function index(){
        $formations = Formation::all();
        return view('formation.index', compact('formations'));
    }

    public function create(){
        $formation = New Formation();
        return view('formation.create', compact('formation'));
    }

    public function store(Request $request){

        $this->validate($request, [
            'formation'=>'required|min:5',
            'description'=>'required|min:5',
            'image'=>'required|mimes:jpg,png',
        ]);

        $image = $request->image;
        $nom_image = uniqid().".".$image->getClientOriginalExtension();
        $image->storeAs('formations', $nom_image, 'public');

        Formation::create([
            "formation" => $request->formation,
            "description" => $request->description,
            "image" => $nom_image,
        ]);

        return redirect()->route('formation.index');
    }

    public function edit(Formation $formation){
        return view('formation.edit', compact('formation'));
    }

    public function update(Request $request, Formation $formation){

        $this->validate($request, [
            'formation'=>'required|min:5',
            'description'=>'required|min:5',
        ]);

        $formation->update([
            "formation" => $request->formation,
            "description" => $request->description,
        ]);

        if($request->image!=""){

            $image = $request->image;
            $nom_image = uniqid().".".$image->getClientOriginalExtension();
            $image->storeAs('formations', $nom_image, 'public');

            $formation->update([
                "image" => $nom_image,
            ]);
        }

        return redirect()->route('formation.index');
    }

    public function destroy(Formation $formation){
        $formation->delete();
        return redirect()->route('formation.index');
    }
}
